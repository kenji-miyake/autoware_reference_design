# Open AD Kit Start Guide

## Overview

This instruction explains how to build development platform for Open AD Kit and run Autoware.Auto on Open AD kit.

## HPC for Open AD Kit is available for purchase

ADLINK provides High Performance Computer(HPC) for Open AD Kit in their website.

- [SOAFEE for Software Defined Vehicles | ADLINK](https://www.adlinktech.com/en/soafee)

Please feel free to contact ADLINK customer support.

- [AVA Developer Platform](https://www.ipi.wiki/pages/com-hpc-altra) is used in this instruction.

## Table of contents

1. [Getting started with EWAOL](getting-started.md)
1. [Boot EWAOL via SSD Boot](boot-ewaol.md)
1. [Extend rootfs partition](extend-rootfs.md)
1. [Checkout Autoware.Auto and Apply Merge Request](checkout-autoware.md)
1. [Setup CI/CD pipeline](cicd.md)
1. [Setup ADE](ade.md)
