# Autoware Reference Design

## Table of contens

1. [Introduction of Reference Design](docs/Introduction-of-Reference-Design/index.md)
2. Operation Environment Requirements
   1. ODD Use Cases
3. System Requirements
   1. Overall Specification
      1. Parity Requirements
   2. Function List
      1. MRM and Diagnostics per Vehicle/ODD
4. System Configurations
   1. System Configuration
   2. Sensor Configuration
   3. Network Configuration
5. Hardware Requirements
   1. Vehicle Requirements
   2. ECU Requirements
   3. Sensor Requirements
      1. LiDAR Requirements
      2. Camera Requirements
      3. Radar Requirements
6. Software Requirements
   1. OS Requirements
      1. Kernel Module Requirements
   2. Middleware Requirements
      1. DDS Requirements
   3. Map Requirements
   4. Design Docs of Autoware.Auto
      1. Architecture
      2. Repository Configuration
      3. Autoware API

- Appendix
  - Hardware List
  - [Open AD Kit Start Guide](docs/Appendix/Open-AD-Kit-Start-Guide/index.md)
  - Design Docs of E2E Simulator
  - Design Docs of Scenario Simulator
